package de.schule.bank.automat;

import de.schule.bank.core.Controller;
import de.schule.bank.core.ServerConnector;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Erstellt von Daniel am 03.07.2016.
 */
public class ControllerAuszahlen extends Controller {
    private final int kontonr;
    @FXML
    public Button zehn;
    @FXML
    public Button zwanzig;
    @FXML
    public Button fuenfzig;
    @FXML
    public Button hundert;
    @FXML
    public Button zweiHundert;
    @FXML
    public Button fuenfHundert;


    public ControllerAuszahlen(Stage s, String konto) {
        super(s, "auszahlen.fxml", "Auszahlen");
        this.kontonr = Integer.parseInt(konto.substring(0, konto.indexOf("-") - 1));

        zehn.setOnAction(event -> zahleAus(10));
        zwanzig.setOnAction(event -> zahleAus(20));
        fuenfzig.setOnAction(event -> zahleAus(50));
        hundert.setOnAction(event -> zahleAus(100));
        zweiHundert.setOnAction(event -> zahleAus(200));
        fuenfHundert.setOnAction(event -> zahleAus(500));
    }

    private void zahleAus(int betrag) {
        if (ServerConnector.getInstance().zahleAus(betrag, kontonr)) {
            new ControllerStartseite(getStage());
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
