package de.schule.bank.automat;

import de.schule.bank.core.Controller;
import de.schule.bank.core.ServerConnector;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Erstellt von Daniel am 03.07.2016.
 */
public class ControllerEinzahlen extends Controller {
    private final int kontonr;
    @FXML
    public Button zehn;
    @FXML
    public Button zwanzig;
    @FXML
    public Button fuenfzig;
    @FXML
    public Button hundert;
    @FXML
    public Button zweiHundert;
    @FXML
    public Button fuenfHundert;


    public ControllerEinzahlen(Stage s, String konto) {
        super(s, "auszahlen.fxml", "Einzahlen");
        this.kontonr = Integer.parseInt(konto.substring(0, konto.indexOf("-") - 1));

        zehn.setOnAction(event -> zahleEin(10));
        zwanzig.setOnAction(event -> zahleEin(20));
        fuenfzig.setOnAction(event -> zahleEin(50));
        hundert.setOnAction(event -> zahleEin(100));
        zweiHundert.setOnAction(event -> zahleEin(200));
        fuenfHundert.setOnAction(event -> zahleEin(500));
    }

    private void zahleEin(int betrag) {
        if (ServerConnector.getInstance().zahleEin(betrag, kontonr)) {
            new ControllerStartseite(getStage());
            return;
        }
        System.err.println("Fehler beim Einzahlen");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
