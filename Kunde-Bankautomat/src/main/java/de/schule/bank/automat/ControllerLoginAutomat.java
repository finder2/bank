package de.schule.bank.automat;

import de.schule.bank.core.Controller;
import de.schule.bank.core.GlobDefines;
import de.schule.bank.core.ServerConnector;
import de.schule.bank.core.vo.SessionHolder;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * Erstellt von user am 21.06.2016.
 */
public class ControllerLoginAutomat extends Controller {

    @FXML
    public TextField kontonr;
    @FXML
    public TextField pin;
    @FXML
    public Button login;
    @FXML
    public Hyperlink register;


    public ControllerLoginAutomat() {
        super(null, "login.fxml", "Login");
    }

    public ControllerLoginAutomat(Stage primaryStage) {
        super(primaryStage, "login.fxml", "Login");


        register.setVisible(false);
        primaryStage.show();
        login.setOnAction((event) -> login());

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new ControllerLoginAutomat(primaryStage);
    }

    private void login() {
        System.out.println("Login versuch");

        ServerConnector connector = ServerConnector.getInstance(GlobDefines.PORT);
        if (connector.login(Integer.parseInt(kontonr.getText()), pin.getText())) {
            new ControllerStartseite(getStage());
            SessionHolder.setPin(pin.getText());
            SessionHolder.setKundennr(Integer.parseInt(kontonr.getText()));
            System.out.println("Erfolgreicher Login");
        }
    }
}
