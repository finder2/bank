package de.schule.bank.automat;

import de.schule.bank.core.Controller;
import de.schule.bank.core.GlobDefines;
import de.schule.bank.core.ServerConnector;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Erstellt von Daniel am 03.07.2016.
 */
public class ControllerStartseite extends Controller {
    @FXML
    public ChoiceBox<String> konten;
    @FXML
    public Button auszahlen;
    @FXML
    public Button einzahlen;


    public ControllerStartseite(Stage s) {
        super(s, "startseite.fxml", "Kontoauswahl");
        ArrayList<String> itemsFrom = ServerConnector.getInstance(GlobDefines.PORT).getKonten();
        ObservableList<String> itemsGui = konten.getItems();
        itemsGui.setAll(itemsFrom);
        konten.setValue(itemsFrom.get(0));
        einzahlen.setOnAction(event -> new ControllerEinzahlen(s, konten.getValue()));
        auszahlen.setOnAction(event -> new ControllerAuszahlen(s, konten.getValue()));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
