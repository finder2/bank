package de.schule.bank.main;

import de.schule.bank.automat.ControllerLoginAutomat;
import javafx.application.Application;

/**
 * Created by Daniel on 25.06.2016.
 */
public class Control {
    public static void main(String[] args) {
        Application.launch(ControllerLoginAutomat.class);
    }
}
