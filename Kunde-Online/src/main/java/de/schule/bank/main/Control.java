package de.schule.bank.main;

import de.schule.bank.core.GlobDefines;
import de.schule.bank.core.ServerConnector;
import de.schule.bank.online.ControllerLogin;
import javafx.application.Application;

/*
 * Created by user on 14.06.2016.
 */
public class Control {
    public static void main(String[] args) {
        System.out.println("min");
        System.out.println("Client gestartet");
        ServerConnector sc = ServerConnector.getInstance(GlobDefines.PORT);
        if (sc == null) {
            return;
        }
        Application.launch(ControllerLogin.class, args);
    }
}
