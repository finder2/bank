package de.schule.bank.online;

import de.schule.bank.core.Controller;
import de.schule.bank.core.ServerConnector;
import de.schule.bank.core.vo.request.transaction.Transaktion;
import de.schule.bank.online.vo.VO_Transaktion;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.print.PrinterJob;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Erstellt von Daniel am 28.06.2016.
 */
public class ControllerKontoauszug extends Controller {
    @FXML
    public ScrollPane content;
    @FXML
    public Button zurueck;
    @FXML
    public Button drucken;

    public ControllerKontoauszug(Stage s) {
        super(s, "Druckvorschau.fxml", "Druckvorschau");

        content.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        content.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        TableView<VO_Transaktion> view = new TableView<>();
        view.setColumnResizePolicy(p -> true);

        ArrayList<Transaktion> transaktions = ServerConnector.getInstance().orderTransaktions();
        ObservableList<VO_Transaktion> vos = view.getItems();
        for (Transaktion t : transaktions) {
            vos.add(new VO_Transaktion(t));
        }


        TableColumn<VO_Transaktion, String> typ = new TableColumn<>("Art");
        typ.setVisible(false);
        typ.setCellValueFactory(cellData -> cellData.getValue().artProperty());
        TableColumn<VO_Transaktion, String> date = new TableColumn<>("Datum");
        date.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
        TableColumn<VO_Transaktion, String> konto = new TableColumn<>("Konto");
        konto.setCellValueFactory(cellData -> cellData.getValue().kontoProperty());
        TableColumn<VO_Transaktion, String> betrag = new TableColumn<>("Betrag");
        betrag.setCellValueFactory(cellData -> cellData.getValue().valueProperty());
        TableColumn<VO_Transaktion, String> message = new TableColumn<>("Beschreibung");
        message.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        view.getColumns().addAll(typ, date, konto, betrag, message);
        view.setItems(vos);
        view.setPrefWidth(s.getWidth());
        view.setEditable(false);
        view.setPrefHeight(s.getHeight());
        view.setPrefWidth(s.getWidth());
        content.setContent(view);
        drucken.setText("Drucken");
        drucken.setOnAction(event1 -> {
            PrinterJob printerJob = PrinterJob.createPrinterJob();
            if (printerJob.showPrintDialog(s.getOwner()) && printerJob.printPage(view))
                printerJob.endJob();
        });
        zurueck.setOnAction(event -> new ControllerStartseite(getStage()));

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
