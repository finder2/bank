package de.schule.bank.online;

import de.schule.bank.core.Controller;
import de.schule.bank.core.GlobDefines;
import de.schule.bank.core.ServerConnector;
import de.schule.bank.core.vo.SessionHolder;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * Erstellt von user am 21.06.2016.
 */
public class ControllerLogin extends Controller {

    @FXML
    public TextField kontonr;
    @FXML
    public TextField pin;
    @FXML
    public Button login;
    @FXML
    public Hyperlink register;


    public ControllerLogin() {
        super(null, "login.fxml", "Login");
    }

    public ControllerLogin(Stage primaryStage) {
        super(primaryStage, "login.fxml", "Login");
        primaryStage.show();
        login.setOnAction((event) -> login());
        register.setOnAction(event -> new ControllerRegister(getStage()));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new ControllerLogin(primaryStage);
    }

    private void login() {
        System.out.println("Login versuch");

        ServerConnector connector = ServerConnector.getInstance(GlobDefines.PORT);
        if (connector.login(Integer.parseInt(kontonr.getText()), pin.getText())) {
            new ControllerStartseite(getStage());
            getStage().show();
            SessionHolder.setPin(pin.getText());
            SessionHolder.setKundennr(Integer.parseInt(kontonr.getText()));
            System.out.println("Erfolgreicher Login");
        }

    }
}
