package de.schule.bank.online;

import de.schule.bank.core.Controller;
import de.schule.bank.core.GlobDefines;
import de.schule.bank.core.ServerConnector;
import de.schule.bank.core.vo.SessionHolder;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Objects;

/**
 * Erstellt von Daniel am 28.06.2016.
 */
public class ControllerPinChange extends Controller {
    @FXML
    public TextField aktuell;
    @FXML
    public TextField neu1;
    @FXML
    public TextField neu2;
    @FXML
    public Button zurueck;
    @FXML
    public Button submit;

    public ControllerPinChange(Stage s) {
        super(s, "NeuePINChild.fxml", "Pinänderung");
        zurueck.setOnAction(event -> new ControllerStartseite(s));
        submit.defaultButtonProperty().bind(submit.focusedProperty());
        submit.setOnAction(event -> {
            if (!Objects.equals(aktuell.getText(), SessionHolder.getPin())) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Fehler bei der Änderung");
                alert.setHeaderText("Die aktuelle Pin stimmt nicht");
                alert.setContentText("Bitte überprüfen Sie Ihre Eingaben");
                alert.show();
                return;
            }
            if (!Objects.equals(neu1.getText(), neu2.getText())) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Fehler bei der Änderung");
                alert.setHeaderText("Die neuen Pins stimmen nicht überein");
                alert.setContentText("Bitte überprüfen Sie Ihre Eingaben");
                alert.show();
                return;
            }
            System.out.println("Länge der neuen pin" + neu1.getText().length());
            if (neu1.getText().length() != 4) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Fehler bei der Änderung");
                alert.setHeaderText("Die neuen Pin muss 4 Stellen haben");
                alert.setContentText("Bitte überprüfen Sie Ihre Eingaben");
                alert.show();
                return;
            }
            if (ServerConnector.getInstance(GlobDefines.PORT).changePIN(neu1.getText())) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Änderung erfolgreich");
                alert.setHeaderText("Pin erfolgreich geändert");
                alert.setContentText("Sie werden auf die Startseite weitergeleitet");
                alert.showAndWait();
                new ControllerStartseite(s);
            }

        });

    }

    public void start(Stage primaryStage) throws Exception {

    }
}