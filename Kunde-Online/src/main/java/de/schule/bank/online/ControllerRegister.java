package de.schule.bank.online;

import de.schule.bank.core.BankHelper;
import de.schule.bank.core.Controller;
import de.schule.bank.core.GlobDefines;
import de.schule.bank.core.ServerConnector;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * Erstellt von Daniel am 30.06.2016.
 */
public class ControllerRegister extends Controller {
    @FXML
    public TextField name;
    @FXML
    public TextField vorname;
    @FXML
    public TextField plz;
    @FXML
    public TextField ort;
    @FXML
    public TextField strasse;
    @FXML
    public TextField nummer;
    @FXML
    public Button zurueck;
    @FXML
    public Button submit;

    public ControllerRegister(Stage stage) {
        super(stage, "Registrieren.fxml", "Registrierung");
        zurueck.setOnAction(event -> new ControllerLogin(stage));
        submit.setOnAction(event -> {
            Pair<Integer, String> credentials;
            try {
                credentials = ServerConnector.getInstance(GlobDefines.PORT).register(name.getText(), vorname.getText(), strasse.getText(), Integer.parseInt(nummer.getText()), plz.getText(), ort.getText());
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warnung");
                alert.setHeaderText("Fehler beim Registrieren");
                alert.setContentText("Bitte geben Sie in den Felder gültige Daten ein");
                alert.show();
                return;
            }
            if (credentials == null) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Registrierung fehlgeschlagen");
                alert.setHeaderText("Fehler bei der Registrierung");
                alert.setContentText("Bitte überprüfen Sie ihre Angaben");
                alert.show();

            } else if (BankHelper.isNumeric(credentials.getValue()) && BankHelper.isNumeric(credentials.getValue())) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Registrierung erfolgreich");
                alert.setHeaderText("Sie bekommen die Zugangsdaten zugeschickt(Siehe Konsole)");
                alert.setContentText("Ihnen wurde ein neues Konto erstellt");
                System.out.println("Kontonr: " + credentials.getKey());
                System.out.println("Pin:" + credentials.getValue());
                alert.showAndWait();
                new ControllerLogin(stage);
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Registrierung fehlgeschlagen");
                alert.setHeaderText("Fehler bei der Registrierung");
                alert.setContentText("Bitte überprüfen Sie ihre Angaben");
                alert.show();
            }
        });

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
