package de.schule.bank.online;

import de.schule.bank.core.Controller;
import de.schule.bank.core.ServerConnector;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Erstellt von user am 26.06.2016.
 */
public class ControllerStartseite extends Controller {
    @FXML
    public Button ueberweisung;
    @FXML
    public Button kontoauszug;
    @FXML
    public Button pin;
    @FXML
    public Button logout;
    @FXML
    public Button konto;

    public ControllerStartseite(Stage primaryStage) {
        super(primaryStage, "startseite.fxml", "Übersicht");
        ueberweisung.setOnAction((event) -> {
            System.out.println("ueberweisung");
            openUberweisung();
        });
        kontoauszug.setOnAction(event -> openKontoauszug());
        pin.setOnAction(event -> openPinChange());
        logout.setOnAction(event -> {
            ServerConnector.getInstance().disconnect();
            new ControllerLogin(getStage());
        });
        konto.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Konto erstellt");
            alert.setHeaderText("Ihre Konto wurde erstellt");
            alert.setContentText("Es hat die Kontonummer: " + ServerConnector.getInstance().createKonto());
            alert.showAndWait();
        });
    }

    private void openPinChange() {
        new ControllerPinChange(getStage());
    }

    private void openKontoauszug() {
        new ControllerKontoauszug(getStage());
    }

    private void openUberweisung() {
        new ControllerUeberweisung(getStage());
    }

    /**
     * soll nicht verwendet werden
     *
     * @param primaryStage --
     * @throws Exception wenn zugegriffen wird
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        throw new IllegalAccessError();
    }
}
