package de.schule.bank.online;

import de.schule.bank.core.BankHelper;
import de.schule.bank.core.Controller;
import de.schule.bank.core.GlobDefines;
import de.schule.bank.core.ServerConnector;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Erstellt von user am 21.06.2016.
 */
public class ControllerUeberweisung extends Controller {
    @FXML
    public ChoiceBox<String> kontoFrom;
    @FXML
    public TextField kontoTo;
    @FXML
    public TextField betrag;
    @FXML
    public TextField zweck;
    @FXML
    public Button submit;
    @FXML
    public Button zurueck;

    public ControllerUeberweisung(Stage s) {
        super(s, "Überweisung.fxml", "Neue Überweisung");

        zurueck.setOnAction(event -> new ControllerStartseite(s));
        submit.setOnAction(event -> ueberweise());
        ArrayList<String> itemsFrom = ServerConnector.getInstance(GlobDefines.PORT).getKonten();
        ObservableList<String> itemsGui = kontoFrom.getItems();
        itemsGui.setAll(itemsFrom);
        kontoFrom.setValue(itemsFrom.get(0));
    }

    private void ueberweise() {
        int from = Integer.parseInt(kontoFrom.getValue().substring(0, kontoFrom.getValue().indexOf("-") - 1));
        int to;
        double value;
        if (kontoTo.getText().length() < 5 || !BankHelper.isNumeric(kontoTo.getText())) {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Fehler bei der Überweisung");
            alert.setHeaderText("Das Ziel Konto ist nicht gültig.");
            alert.setContentText("Bitte überprüfen Sie ihre Eingabe.");
            alert.show();
            return;
        } else {
            to = Integer.parseInt(kontoTo.getText());
        }
        if (!BankHelper.isNumeric(betrag.getText()) || !BankHelper.isMoney(betrag.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Fehler bei der Überweisung");
            alert.setHeaderText("Der Betrag ist nicht gültig");
            alert.setContentText("Bitte überprüfen Sie ihre Eingabe.");
            alert.show();
            return;
        } else {
            value = BankHelper.parseMoney(betrag.getText());
        }
        if (zweck.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Fehler bei der Überweisung");
            alert.setHeaderText("Der Verwendungszweck ist nicht gültig");
            alert.setContentText("Bitte überprüfen Sie ihre Eingabe.");
            alert.show();
            return;
        }
        if (ServerConnector.getInstance().ueberweise(from, to, value, zweck.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Überweisung erfolgreich");
            alert.setHeaderText("Die Überweisung wurde erfolgreich getätigt");
            alert.setContentText("Sie werden zur Startseite weitergeleitet");
            alert.showAndWait();
            new ControllerStartseite(getStage());
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
