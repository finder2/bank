package de.schule.bank.online.vo;

import de.schule.bank.core.vo.request.Request;
import de.schule.bank.core.vo.request.transaction.Transaktion;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Erstellt von Daniel am 03.07.2016.
 */
public class VO_Transaktion extends Request {
    private final StringProperty art;
    private final StringProperty date;
    private final StringProperty konto;
    private final StringProperty value;
    private final StringProperty description;

    public VO_Transaktion(Transaktion t) {
        art = new SimpleStringProperty(t.getClass().getSimpleName());
        date = new SimpleStringProperty(t.getDate());
        konto = new SimpleStringProperty("" + t.getKonto());
        value = new SimpleStringProperty(String.valueOf(t.getBetrag()));
        description = new SimpleStringProperty(t.getDescription());
    }

    public StringProperty artProperty() {
        return art;
    }

    public StringProperty dateProperty() {
        return date;
    }

    public StringProperty kontoProperty() {
        return konto;
    }

    public StringProperty valueProperty() {
        return value;
    }

    public StringProperty descriptionProperty() {
        return description;
    }
}
