package de.schule.bank.main;

import de.schule.bank.mitarbeiter.ControllerStartseite;
import javafx.application.Application;

/**
 * Erstellt von Daniel am 05.07.2016.
 */
public class Control {
    public static void main(String[] args) {
        Application.launch(ControllerStartseite.class, args);
    }
}
