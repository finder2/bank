package de.schule.bank.mitarbeiter;

import de.schule.bank.core.Controller;
import de.schule.bank.core.ServerConnector;
import de.schule.bank.core.vo.TO_Konto;
import de.schule.bank.core.vo.TO_Kunde;
import de.schule.bank.core.vo.VO_Konto;
import de.schule.bank.core.vo.VO_Kunde;
import de.schule.bank.core.vo.request.MitarbeiterView;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.stage.Stage;

/**
 * Erstellt von Daniel am 05.07.2016.
 */
public class ControllerStartseite extends Controller {
    @FXML
    public Accordion accordion;
    @FXML
    public Button submit;
    @FXML
    Button reload;

    TableView<TO_Konto> tv_konto = new TableView<>();
    TableView<TO_Kunde> tv_kunden = new TableView<>();
    MitarbeiterView view;

    public ControllerStartseite() {
        super(null, null, null);
    }

    public ControllerStartseite(Stage s) {
        super(s, "startseite.fxml", "MitarbeiterClient");
        //Komponenten anlegen
        TitledPane tp_konten = new TitledPane();
        TitledPane tp_kunden = new TitledPane();


        //Spalten für die Konten anlegen
        TableColumn<TO_Konto, String> tc_kontonr = new TableColumn<>("Kontonummer");
        TableColumn<TO_Konto, String> tc_value = new TableColumn<>("Kontostand");
        TableColumn<TO_Konto, Boolean> tc_kontoActive = new TableColumn<>("Aktiv");

        //Wertverknüpfung erstellen
        tc_kontonr.setCellValueFactory(cellData -> cellData.getValue().kontoNrProperty());
        tc_value.setCellValueFactory(cellData -> cellData.getValue().kontostandProperty());
        tc_kontoActive.setCellValueFactory(cellData -> cellData.getValue().activeProperty());
        tc_kontoActive.setCellFactory(param -> new CheckBoxTableCell<>());
        tc_kontoActive.setEditable(true);

        //spalten hinzufügen
        tv_konto.getColumns().addAll(tc_kontonr, tc_value, tc_kontoActive);
        tv_konto.setEditable(true);
        tp_konten.setContent(tv_konto);
        tp_konten.setText("Konten");

        //Spalten für Kunden erstellen
        TableColumn<TO_Kunde, String> tc_kundennr = new TableColumn<>("Kundennummer");
        TableColumn<TO_Kunde, String> tc_name = new TableColumn<>("Name");
        TableColumn<TO_Kunde, String> tc_vorname = new TableColumn<>("Vorname");
        TableColumn<TO_Kunde, String> tc_strasse = new TableColumn<>("Straße");
        TableColumn<TO_Kunde, String> tc_nr = new TableColumn<>("Nr.");
        TableColumn<TO_Kunde, String> tc_plz = new TableColumn<>("PLZ");
        TableColumn<TO_Kunde, String> tc_ort = new TableColumn<>("Ort");
        TableColumn<TO_Kunde, Boolean> tc_locked = new TableColumn<>("Gesperrt");
        TableColumn<TO_Kunde, Boolean> tc_kundeActive = new TableColumn<>("Aktiv");

        //Wertverknüpfung erstellen
        tc_kundennr.setCellValueFactory(cellData -> cellData.getValue().kundenNrProperty());
        tc_name.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        tc_vorname.setCellValueFactory(cellData -> cellData.getValue().vornameProperty());
        tc_strasse.setCellValueFactory(cellData -> cellData.getValue().strasseProperty());
        tc_nr.setCellValueFactory(cellData -> cellData.getValue().nrProperty());
        tc_plz.setCellValueFactory(cellData -> cellData.getValue().plzProperty());
        tc_ort.setCellValueFactory(cellData -> cellData.getValue().ortProperty());

        tc_locked.setCellValueFactory(cellData -> cellData.getValue().lockedProperty());
        tc_locked.setCellFactory(tc -> new CheckBoxTableCell<>());
        tc_locked.setEditable(true);
        tc_kundeActive.setCellValueFactory(cellData -> cellData.getValue().activeProperty());
        tc_kundeActive.setCellFactory(tc -> new CheckBoxTableCell<>());
        tc_kundeActive.setEditable(true);

        tv_kunden.getColumns().addAll(tc_kundennr, tc_name, tc_vorname, tc_strasse, tc_nr, tc_plz, tc_ort, tc_locked, tc_kundeActive);
        tv_kunden.setEditable(true);
        tp_kunden.setContent(tv_kunden);
        tp_kunden.setText("Kunden");

        loadContent();
        submit.setOnAction(event -> {
            view.setKonten(tv_konto.getItems());
            view.setKunden(tv_kunden.getItems());
            if (ServerConnector.getInstance().refreshKonten(view)) {
                Alert al = new Alert(Alert.AlertType.INFORMATION);
                al.setTitle("Erolgreiche Übertagung");
                al.setHeaderText("Daten erfolgreich Übertragen");
                al.setContentText("server hat nun den Aktuellen stand der Konten und Kunden");
                al.show();
            }
            loadContent();
        });
        reload.setOnAction(event -> loadContent());
        accordion.getPanes().addAll(tp_kunden, tp_konten);
        accordion.setExpandedPane(tp_kunden);
        s.show();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new ControllerStartseite(primaryStage);
    }

    private void loadContent() {
        //Tabellen mit Werten füllen
        view = ServerConnector.getInstance().getKontenObjekte();
        if (view == null) {
            return;
        }
        tv_konto.getItems().clear();
        for (VO_Konto konto : view.getKonten()) {
            tv_konto.getItems().add(new TO_Konto(konto.kontoNr(), konto.kontostand(), konto.active()));
        }
        tv_kunden.getItems().clear();
        for (VO_Kunde kunde : view.getKunden()) {
            tv_kunden.getItems().add(new TO_Kunde(kunde.kundenNr(), kunde.name(), kunde.vorname(), kunde.strasse(), kunde.nr(), kunde.plz(), kunde.ort(), kunde.locked(), kunde.active()));
        }

    }
}
