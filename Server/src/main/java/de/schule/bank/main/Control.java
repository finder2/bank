package de.schule.bank.main;

import de.schule.bank.core.GlobDefines;
import de.schule.bank.server.Server;

/**
 * Erstellt von user am 03.05.2016.
 */
public class Control {
    public static void main(String... args) {
        final int port;
        if (args.length == 1) {
            port = Integer.parseInt(args[0]);
        } else {
            port = GlobDefines.PORT;
        }
        Server s = new Server(port);
        s.start();
    }
}
