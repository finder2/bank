package de.schule.bank.server;

import de.schule.bank.core.vo.request.transaction.Transaktion;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Erstellt von user am 10.05.2016.
 */
public class Konto implements Serializable {
    private static final long serialVersionUID = 0L;

    private final Integer kontoNr;
    private final ArrayList<Transaktion> historie;
    private Double kontostand;
    private Boolean active;

    public Konto() {
        this.kontoNr = OnlineBank.getInstance().getMin_kontonr();
        this.kontostand = 0d;
        historie = new ArrayList<>();
        active = false;
        OnlineBank.getInstance().addKonto(this);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Konto)) return false;
        Konto otherKonto = (Konto) other;
        return otherKonto.getKontoNr() == kontoNr;
    }


    public String toString() {
        return kontoNr + " Guthaben: " + kontostand;
    }

    public void einzahlen(double betrag) {
        kontostand = kontostand + betrag;
    }

    public boolean auszahlen(double betrag) {
        if (betrag > kontostand) {
            return false;
        } else {
            kontostand = kontostand - betrag;
            return true;
        }
    }

    public Double getKontostand() {
        return kontostand;
    }

    public int getKontoNr() {
        return kontoNr;
    }

    public void addHistory(Transaktion trans) {
        historie.add(trans);
    }

    public ArrayList<Transaktion> getHistorie() {
        return historie;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = true;
    }


}
