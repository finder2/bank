package de.schule.bank.server;

import de.schule.bank.core.GlobDefines;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Erstellt von user am 10.05.2016.
 */
public class Kunde implements Serializable {
    private static final long serialVersionUID = 0L;

    private final Integer kundenNr;
    private final String name;
    private final String vorname;
    private final String strasse;
    private final Integer nr;
    private final String plz;
    private final String ort;
    private final ArrayList<Konto> konten;
    private Integer wrongPin = 0;
    private String pin;
    private Boolean locked = false;
    private Boolean active = false;

    public Kunde(String name, String vorname, String strasse, int nr, String plz, String ort, String pin, boolean newKonto) {
        super();
        this.kundenNr = OnlineBank.getInstance().getMin_kundennr();
        this.name = name;
        this.vorname = vorname;
        this.pin = pin;
        this.konten = new ArrayList<>();
        this.strasse = strasse;
        this.nr = nr;
        this.plz = plz;
        this.ort = ort;
        System.out.println("kundennr: " + kundenNr);
        if (newKonto) {
            konten.add(new Konto());
        }
        active = false;
    }

    @Override
    public String toString() {
        return "Kunde " + kundenNr + " hat die Pin " + pin;
    }

    /**
     * @return gibt den Namen zurück
     */
    public String getName() {
        return name;
    }

    /**
     * @return gibt die Kundennnummer zurück
     */
    public int getKundenNr() {
        return kundenNr;
    }

    /**
     * @return gibt die Pin zurück;
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin setzt die Pin des Kunden
     */
    public void setPin(String pin) {
        if (pin != null && pin.length() == 4) {
            this.pin = pin;
            OnlineBank.getInstance().save();
        } else {
            throw new IllegalArgumentException("Pin ist nicht gültig");
        }
    }

    /**
     * @return gibt die Konten des Kunden zurück
     */
//    public ArrayList<Konto> getKonten() {
//        return konten;
//    }

   /* /**
     * fügt dem Kunden ein Konto hinzu
     *
     * @param k
     */
//    public void addKonto(Konto k) {
//        konten.add(k);
//        OnlineBank.getInstance().save();
//    }
    public String getVorname() {
        return vorname;
    }

    public String getStrasse() {
        return strasse;
    }

    public int getNr() {
        return nr;
    }

    public String getPlz() {
        return plz;
    }

    public String getOrt() {
        return ort;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
        OnlineBank.getInstance().save();
    }

    public void resetLogin() {
        wrongPin = 0;
    }

    public void addWrongLogin() {
        wrongPin++;
        if (wrongPin > GlobDefines.WRONG_PASSWORD_COUNT) {
            locked = true;
            resetLogin();
        }
        OnlineBank.getInstance().save();
    }

    public ArrayList<Konto> getKonten() {
        return konten;
    }

    public ArrayList<String> getKontenAsString() {
        ArrayList<String> value = new ArrayList<>();
        for (Konto k : konten) {
            value.add(k.toString());
        }
        return value;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive() {
        active = true;
    }

    public void addKonto(Konto konto) {
        konten.add(konto);
    }
}
