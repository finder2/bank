package de.schule.bank.server;

import de.schule.bank.core.vo.request.transaction.Auszahlen;
import de.schule.bank.core.vo.request.transaction.Einzahlen;
import de.schule.bank.core.vo.request.transaction.Ueberweisung;

import java.io.*;
import java.util.ArrayList;

/**
 * Erstellt von Daniel Brenzel am 10.05.2016.
 */
public class OnlineBank {
    private static final File kontenFile = new File("." + File.separator + "konten.obj");
    private static final File kundenFile = new File("." + File.separator + "kunden.obj");
    private static OnlineBank instance = null;
    private int min_kontonr = 10000;
    private int min_kundennr = 10000;
    private ArrayList<Konto> konten;
    private ArrayList<Kunde> kunden;

    /**
     * Konstruktor der Klasse OnlineBank
     */
    public OnlineBank() {
        konten = new ArrayList<>();
        kunden = new ArrayList<>();
        if (!kontenFile.exists()) {
            System.out.println("konten datei muss angelegt werden");
            try {
                kontenFile.createNewFile();
                save(konten, kontenFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!kundenFile.exists()) {
            System.out.println("kunden datei muss angelegt werden");
            try {
                kundenFile.createNewFile();
                save(kunden, kundenFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        load();
    }

    public static OnlineBank getInstance() {
        if (instance == null) {
            instance = new OnlineBank();
        }
        return instance;
    }

    public synchronized void addKunde(Kunde k) {
        kunden.add(k);
        //Kunde hinzugefügt
        save();
    }

    public synchronized Kunde loginKunde(int kundenNr, String pin) throws NotActiveException {
        System.out.println("login beginn " + kundenNr + "pin" + pin);
        System.out.println(kunden.size() + " verfügbar");
        for (Kunde k : kunden) {
            if (k.getKundenNr() == kundenNr) {
                if (k.isLocked() || !k.isActive()) {
                    System.out.println("Locked");
                    throw new NotActiveException();
                }
                if (k.getPin().equals(pin)) {
                    System.out.println("Richtiger Login");
                    return k;
                } else {
                    System.out.println("Falscher Login");
                    k.addWrongLogin();
                    save();
                }
            }
        }
        return null;
    }

    /**
     * Helper Methode die die Listen ins Dateisystem schreibt
     */
    public synchronized void save() {
        System.out.println("speichere");
        save(konten, kontenFile);
        save(kunden, kundenFile);
    }

    private <T> void save(ArrayList<T> list, File f) {
        System.out.println("speicher Datei");
        try {
            System.out.println("alte datei gelöscht");
            f.createNewFile();
            FileOutputStream fout = new FileOutputStream(f);
            ObjectOutputStream oout = new ObjectOutputStream(fout);
            oout.writeObject(list);
            System.out.println("neue Datei erstellt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Lädt alle Listen aus dem Dateisystem und speichert sie in Variablen
     */
    public synchronized void load() {
        System.out.println("lade");
        konten = load(Konto.class, kontenFile);
        kunden = load(Kunde.class, kundenFile);
        refreshMinValues();
    }

    /**
     * lädt eine Liste aus dem Datei system
     *
     * @param f   Datei aus der das Objekt gelsen wird
     * @param <T> Typ der zurück zugebenden liste
     * @return die Liste aus dem Dateisystem
     */
    private <T> ArrayList<T> load(Class<T> type, File f) {
        if (f.exists()) {
            try {
                FileInputStream fin = new FileInputStream(f);
                ObjectInputStream oin = new ObjectInputStream(fin);
                Object o = oin.readObject();
                return (ArrayList<T>) o;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * aktuallisiert die Aktuell niedrigste kontonummer
     */
    public void refreshMinValues() {
        for (Konto k : konten) {
            System.out.println("Konto gefunden");
            min_kontonr = Integer.max(min_kontonr + 1, k.getKontoNr());
        }
        for (Kunde k : kunden) {
            System.out.println("kunde gefunden");
            min_kundennr = Integer.max(min_kundennr + 1, k.getKundenNr());
        }
    }

    /**
     * Überweist einen Betrag auf ein Konto uns speichert den Betrag in der Historie
     *
     * @param u das Historienobjekt aus dem man die Benötigten werte lesen kann
     */
    public synchronized boolean ueberweise(Ueberweisung u) {
        Konto von = null;
        Konto zu = null;

        for (Konto k : konten) {
            if (k.getKontoNr() == u.getKontonrVon()) {
                von = k;
            }
            if (k.getKontoNr() == u.getKontonrZu()) {
                zu = k;
            }
        }
        if (von != null && zu != null) {
            von.auszahlen(u.getBetrag());
            von.addHistory(u);
            if (von.getKontoNr() != zu.getKontoNr()) {
                zu.einzahlen(u.getBetrag());
                zu.addHistory(new Ueberweisung(u.getKontonrZu(), u.getKontonrVon(), u.getBetrag(), u.getVerwendungszweck(), null));
            }
        } else {
            return true;
        }
        save();
        return true;
    }

    /**
     * Gibt die nächste freie Kontonummer zurück
     *
     * @return die nächste freie Kontonummer
     */
    public int getMin_kontonr() {
        refreshMinValues();
        return min_kontonr;
    }

    /**
     * Gibt die nächste freie Kundennummer zurück
     *
     * @return die nächste freie Kundennummer
     */
    public int getMin_kundennr() {
        refreshMinValues();
        return min_kundennr;
    }

    /**
     * fügt ein Konto dem System hinzu.
     *
     * @param konto neues Konto
     */
    public synchronized void addKonto(Konto konto) {
        konten.add(konto);
        save();
    }

    /**
     * Zahl einen Betrag auf ein Konto ein. Informationen kommen aus dem empfangen Objekt
     *
     * @param ein Historyobjekt der Einzahlung
     */
    public synchronized void zahleEin(Einzahlen ein) {
        for (Konto k : konten) {
            if (ein.getKonto() == k.getKontoNr()) {
                System.out.println("Einzahlen von " + ein.getBetrag() + "  erfolgreich");
                k.einzahlen(ein.getBetrag());
                k.addHistory(ein);
                save();
                return;
            }
        }
    }

    public synchronized boolean zahleAus(Auszahlen a) {
        for (Konto k : konten) {
            if (a.getKonto() == k.getKontoNr() && k.auszahlen(a.getBetrag())) {
                k.addHistory(a);
                save();
                return true;
            }
        }
        return false;
    }

    /**
     * Wenn im KundenObjekt nicht die Richtigen konten stehen einfach rüber kopieren
     *
     * @param kontenAlt Konto aus dem Kunden objekt
     * @return aktuelle Kontenobjekte
     */
    public ArrayList<Konto> syncKonten(ArrayList<Konto> kontenAlt) {
        ArrayList<Konto> result = new ArrayList<>();
        for (Konto k : konten) {
            for (Konto kAlt : kontenAlt) {
                if (k.equals(kAlt)) {
                    result.add(k);
                }
            }
        }
        return result;
    }

    public ArrayList<Konto> getKonten() {
        return konten;
    }

    public ArrayList<Kunde> getKunden() {
        return kunden;
    }

    public void addKonto(Konto konto, Kunde kunde) {
        kunde.addKonto(konto);
    }
}