package de.schule.bank.server;

import de.schule.bank.core.vo.TO_Konto;
import de.schule.bank.core.vo.TO_Kunde;
import de.schule.bank.core.vo.VO_Konto;
import de.schule.bank.core.vo.VO_Kunde;
import de.schule.bank.core.vo.request.*;
import de.schule.bank.core.vo.request.transaction.Auszahlen;
import de.schule.bank.core.vo.request.transaction.Einzahlen;
import de.schule.bank.core.vo.request.transaction.Transaktion;
import de.schule.bank.core.vo.request.transaction.Ueberweisung;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Erstellt von user am 10.05.2016.
 */
public class Server {
    private int port;
    private ServerSocket s;

    public Server(int port) {
        this.port = port;
    }

    public void start() {
        System.out.println("Server auf port " + port + " gestartet");
        try {
            s = new ServerSocket(port);
            while (true) {
                System.out.println("Warte auf eingehende Verbindung");
                Socket client = s.accept();
                System.out.println("Verbindung angenommen. Wird verarbeitet");
                Thread t = new Thread(() -> {
                    handleClient(client);
                });
                t.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void handleClient(Socket client) {
        try {
            InputStream in = client.getInputStream();
            OutputStream out = client.getOutputStream();
            ObjectInputStream oin = new ObjectInputStream(in);
            ObjectOutputStream oout = new ObjectOutputStream(out);
            Session s = new Session(oout);
            while (true) {
                try {
                    Object o = oin.readObject();
                    System.out.println("Objekt erhalten");
                    if (o != null && parseObject(o, s)) {
                        System.out.println("Verbindung mit " + s + " wurde beendt.");
                        break;
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    break;
                }
            }
            in.close();
            out.close();
            oin.close();
        } catch (SocketException e) {
            if (Objects.equals(e.getMessage(), "Connection reset")) {
                System.out.println("Client hat sich abgemeldet");
            } else {
                e.printStackTrace();
            }
        } catch (EOFException e) {
            System.out.println("logout");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return true wenn beendet wird ansonsten false
     */
    private boolean parseObject(Object o, Session s) {
        System.out.println("Objekt; " + o.getClass().getSimpleName());
        System.out.println("session : " + s.getKunde());
        //Mitarbeiter braucht keinen Login (würde in der Realität anhand der IP überprüft werden)
        if (o instanceof MitarbeiterView) {
            System.out.println("mitarbeiter view erhalten");
            MitarbeiterView v = (MitarbeiterView) o;
            System.out.println("Konten: " + v.getKonten().size());
            System.out.println("Kunden: " + v.getKunden().size());
            if (v.getKonten().size() != 0 || v.getKunden().size() != 0) {
                s.writeObject(new OK(0));
                //Konten durchgehen und Werte übernehmen
                for (VO_Konto voK : v.getKonten()) {
                    for (Konto k : OnlineBank.getInstance().getKonten()) {
                        if (voK != null) {
                            System.out.println("vo ist " + voK.active() + " und hat einen Konto stand von " + voK.kontostand());
                            k.setActive(voK.active());
                        }
                    }
                }
                //selbes Spielchen mit kunden
                for (VO_Kunde voK : v.getKunden()) {
                    OnlineBank.getInstance().getKunden().stream().filter(k -> voK.kundenNr().equals(k.getKundenNr())).forEach(k -> {
                        if (voK.active()) {
                            k.setActive();
                        }
                        k.setLocked(voK.locked());
                    });
                }
            } else {
                ArrayList<Konto> konten = OnlineBank.getInstance().getKonten();
                ArrayList<Kunde> kunden = OnlineBank.getInstance().getKunden();
                for (Konto k : konten) {
                    v.addKonto(new TO_Konto(k.getKontoNr(), k.getKontostand(), k.isActive()));
                }
                for (Kunde k : kunden) {
                    v.addKunde(new TO_Kunde(k.getKundenNr(), k.getName(), k.getVorname(), k.getStrasse(), k.getNr(), k.getPlz(), k.getOrt(), k.isLocked(), k.isActive()));
                }
                s.writeObject(v);
            }
        } else if (o instanceof Login) {
            if (s.getKunde() != null) {
                System.out.println("login: " + s.getKunde().getKundenNr() + " mit Pin:" + s.getKunde().getPin());
            }
            Login l = (Login) o;
            Kunde k = null;
            try {
                k = OnlineBank.getInstance().loginKunde(l.getKundennr(), l.getPin());
            } catch (NotActiveException e) {
                s.writeObject(new Exit());
            }
            if (k != null) {
                s.setKunde(k);
                s.writeObject(new OK(-1));
            } else {
                s.writeObject(new Fail());
            }
            return false;
        } else if (o instanceof Register) {
            System.out.println("registriere");
            Register r = (Register) o;
            final Kunde k = new Kunde(r.getName(), r.getVorname(), r.getStraße(), r.getNr(), r.getPlz(), r.getOrt(), r.getPin(), true);
            OnlineBank.getInstance().addKunde(k);
            s.writeObject(new OK(k.getKundenNr()));
        } else if (!s.isValid()) {
            System.out.println("Anfrage wegen flaschen logins abgelehnt");
            return false;
        }

        if (o instanceof PinChange) {
            PinChange p = (PinChange) o;
            s.getKunde().setPin(p.getPin());
            s.writeObject(p);
        } else if (o instanceof Exit) {
            s.setValid(false);
            return true;
        } else if (o instanceof Ueberweisung) {
            Ueberweisung u = (Ueberweisung) o;
            if (u.getDate() == null && u.getVerwendungszweck() == null) {
                ArrayList<String> kontenGui = new ArrayList<>();
                if (s.getKunde() != null) {
                    ArrayList<Konto> konten = s.getKunde().getKonten();
                    konten = OnlineBank.getInstance().syncKonten(konten);
                    for (Konto k : konten) {
                        kontenGui.add(k.getKontoNr() + " - Guthaben: " + k.getKontostand());
                    }
                }

                s.writeObject(new Ueberweisung(kontenGui));
            } else {
                if (OnlineBank.getInstance().ueberweise(u)) {
                    s.writeObject(new OK(-1));
                } else {
                    s.writeObject(new Fail());
                }
            }
        } else if (o instanceof Auszahlen) {
            System.out.println("Zahle aus");
            Auszahlen a = (Auszahlen) o;
            if (OnlineBank.getInstance().zahleAus(a)) {
                s.writeObject(new OK(a.getKonto()));
            } else {
                s.writeObject(new Fail());
            }
        } else if (o instanceof Einzahlen) {
            System.out.println("Zahle ein");
            Einzahlen ein = (Einzahlen) o;
            OnlineBank.getInstance().zahleEin(ein);
            s.writeObject(new OK(ein.getKonto()));
        } else if (o instanceof Pin) {
            Pin p = (Pin) o;
            s.getKunde().setPin(p.getNewPin());
            s.writeObject(p);
        } else if (o instanceof Print) {
            Print p = new Print();
            ArrayList<Konto> userKonten = OnlineBank.getInstance().syncKonten(s.getKunde().getKonten());
            for (Konto k : userKonten) {
                for (Transaktion trans : k.getHistorie()) {
                    p.addTransaktion(trans);
                }
            }
            System.out.println("Transaktionen: " + p.getHistory().size());
            System.out.println("objekt geschrieben");
            s.writeObject(p);
        } else if (o instanceof KontoRequest) {
            Konto k = new Konto();
            OnlineBank.getInstance().addKonto(k, s.getKunde());
            s.writeObject(new OK(k.getKontoNr()));
        }
        return false;
    }


}