package de.schule.bank.server;

import de.schule.bank.core.vo.request.Request;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 */
public class Session extends Request {
    private final transient ObjectOutputStream out;
    private Kunde kunde = null;
    private ArrayList<Konto> konten = null;
    private Boolean valid = false;

    /**
     * Erstellt eine neue Session
     */
    public Session(ObjectOutputStream out) {
        this.out = out;
        this.konten = new ArrayList<>();
    }

    public String toString() {
        if (kunde != null) {
            return "Session des Kunden mit id: " + kunde.getKundenNr();
        } else {
            return "ungültige session";
        }
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde k) {
        kunde = k;
        valid = k != null;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean newValue) {
        valid = newValue;
    }

    public void writeObject(Request o) {
        try {
            out.writeObject(o);
            out.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

