package de.schule.bank.core;

import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Erstellt von Daniel am 01.07.2016.
 */
public class BankHelper {
    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?") || str.matches("-?\\d+(,\\d+)?");
    }

    /**
     * gibt true zurück wenn es ein gültiger geldwert ist.
     *
     * @param money der wert der geprüft werden soll
     * @return true wenn es nicht mehr als 2 Dezimalstellen hat ansonsten false
     */
    public static boolean isMoney(String money) {
        String s = String.valueOf(money);
        return s.matches("-?\\d+(\\.)?(\\d){0,2}") || s.matches("-?\\d+(,)?(\\d){0,2}");
    }

    public static Double parseMoney(String money) {
        if (isNumeric(money)) {
            if (money.contains(",")) {
                NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
                Number number;
                try {
                    number = format.parse(money);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
                return number.doubleValue();
            } else {
                try {
                    return Double.parseDouble(money);
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        return null;
    }

    public static void kontoEmpty() {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Fehler bei der Überweisung");
            alert.setHeaderText("Das Konto ist nicht gedeckt");
            alert.setContentText("Bitte sorgen sie vor einer Überweisung für eine ausreichende Deckung des Kontos");
            alert.show();
        });
    }
}
