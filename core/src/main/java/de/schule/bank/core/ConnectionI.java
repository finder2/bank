package de.schule.bank.core;

import java.rmi.Remote;

/**
 * Created by Daniel on 29.06.2016.
 */
public interface ConnectionI extends Remote {
    boolean login(int kundenNr, String pin);
}
