package de.schule.bank.core;

import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Daniel on 26.06.2016.
 */
public abstract class Control implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
