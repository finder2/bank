package de.schule.bank.core;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

/**
 * Erstellt von user am 21.06.2016.
 */
public abstract class Controller extends Application {
    private Stage main;

    /**
     * Lädt ein FXML aus der JAR oder dem Verzeichnis in dem die JAR ausgeführt wurde und zeigt diese in der übergebenen Stage an.
     *
     * @param primaryStage    Gui auf die der Inhalt gezeichnet werden soll
     * @param filename        Dateiname des FXML (Muss im root der Jar liegen)
     * @param applicationName Name des Fensters
     */
    public Controller(Stage primaryStage, String filename, String applicationName) {
        if (primaryStage != null) {
            main = primaryStage;
            try {
                FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(filename));
                loader.setController(this);
                Parent root = loader.load();
                Scene scene = new Scene(root);
                primaryStage.setTitle(applicationName);
                primaryStage.setScene(scene);
                primaryStage.setOnCloseRequest(event -> {
                    System.out.println("Kreuzchen geklickt. Dialog anzeigen");
                    ButtonType yes = new ButtonType("Ja", ButtonBar.ButtonData.YES);
                    ButtonType no = new ButtonType("Nein", ButtonBar.ButtonData.CANCEL_CLOSE);

                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Beenden der Appliaktion");
                    alert.setHeaderText("Wollen Sie die Applikation wirklich beenden?");
                    alert.setContentText("Ihre eingegebenen Informationen könnten verloren gehen.");
                    alert.getButtonTypes().setAll(yes, no);
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == yes) {
                        System.out.println("Beenden der Applikation");
                        System.exit(0);
                    } else {
                        event.consume();
                    }

                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * gibt die Stage zurück
     *
     * @return die Im Load übergeben Stage
     */
    public Stage getStage() {
        return main;
    }
}
