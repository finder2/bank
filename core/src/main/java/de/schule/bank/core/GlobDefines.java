package de.schule.bank.core;

/**
 * Created by user on 10.05.2016.
 */
public class GlobDefines {
    public static final int WRONG_PASSWORD_COUNT = 3;
    public static final String HOST = "localhost";
    public static final int PORT = 8000;
    public static final String RMI_STRING = "Connection";
    public static final int MIN_KONTONR = 10000;
    public static final int MIN_KUNDEN_NR = 10000;
}
