package de.schule.bank.core;

import de.schule.bank.core.vo.VO_Konto;
import de.schule.bank.core.vo.request.*;
import de.schule.bank.core.vo.request.transaction.Auszahlen;
import de.schule.bank.core.vo.request.transaction.Einzahlen;
import de.schule.bank.core.vo.request.transaction.Transaktion;
import de.schule.bank.core.vo.request.transaction.Ueberweisung;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Alert;
import javafx.util.Pair;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Erstellt von user am 21.06.2016.
 */
public class ServerConnector {

    private static ServerConnector instance;
    private static Socket s;
    private static ObjectOutputStream oout;
    private static ObjectInputStream oin;
    private static InputStream in;
    private static OutputStream out;
    private final int port;
    private ArrayList<Consumer<Request>> listners;


    public ServerConnector(int port) throws IOException {
        this.port = port;
        listners = new ArrayList<>();
        reconnect();
    }

    private static void alert() {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Verbindungsfehler");
            alert.setHeaderText("Fehler bei der Verbindung zum Server");
            alert.setContentText("Bitte überprüfen Sie Ihre Netzwerkverbindung. Wenn dieses Problem weiterhin auftauchen sollte kontaktieren Sie bitte den Entwickler");
            alert.showAndWait();
            System.exit(1);
        });
    }

    /**
     * Erzeugt eine neue Instanz
     *
     * @return die neue Instanz
     */
    public static ServerConnector getInstance(int port) {
        if (instance == null) {
            System.out.println("verbindung auf Port " + port + " versucht");
            for (int i = 0; i <= 3; i++) {
                try {
                    instance = new ServerConnector(port);
                    return instance;
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            alert();
        }
        return instance;

    }

    /**
     * Erzeugt eine neue Instanz
     *
     * @return die neue Instanz
     */
    public static ServerConnector getInstance() {
        return getInstance(GlobDefines.PORT);
    }

    private synchronized void reconnect() {
        try {
            s = new Socket("localhost", port);
            out = s.getOutputStream();
            oout = new ObjectOutputStream(out);
            in = s.getInputStream();
            oin = new ObjectInputStream(in);
        } catch (IOException e) {
            System.out.println("verbindung fehlgeschlagen");
            s = null;

        }
        if (s != null) {
            Thread t = new Thread(() -> {
                try {
                    while (true) {
                        try {
                            Object o = oin.readObject();
                            System.out.println("Objekt erhalten: " + o.getClass().getCanonicalName());
                            if (o instanceof Request) {
                                synchronized (this) {
                                    for (Consumer<Request> c : listners) {
                                        c.accept((Request) o);
                                    }
                                }
                            }
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                    in.close();
                    out.close();
                    oin.close();
                } catch (IOException e) {
                    if (instance != null) {
                        alert();
                    }
                }
            });
            t.setName("ServerConnectionThread");
            t.start();
        }
    }

    /**
     * Fügt einen Listner zur Serververbindung hinzu und löscht ihn nach einer Sekunde.
     *
     * @param listner Wird aufgerufen wenn eine Nachricht vom Server empfangen wurde
     */
    public synchronized void addListner(Consumer<Request> listner) {
        listners.add(listner);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                removeListner(listner);
            }
        };
        Timer t = new Timer(false);
        t.schedule(task, 1000L);
    }

    /**
     * Löscht den listner
     *
     * @param listner Listner der entfernt werden soll
     */
    private synchronized void removeListner(Consumer<Request> listner) {
        listners.remove(listner);
    }

    /**
     * Loggt den Kunden ein
     *
     * @param kundennr des Kunden
     * @param pin      des Kunden
     * @return true wenn erfolgreich ansonst false
     */
    public boolean login(int kundennr, String pin) {
        if (!checkConnection()) {
            return false;
        }
        Login login = new Login(kundennr, pin);
        try {
            oout.writeObject(login);
            oout.reset();
            BooleanProperty locked = new SimpleBooleanProperty(false);
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consume = (Request r) -> {
                if (r instanceof OK) {
                    System.out.println("login  erfolgreich");
                    doneSignal.countDown();
                } else if (r instanceof Exit) {
                    Platform.runLater(() -> {
                        Alert l = new Alert(Alert.AlertType.WARNING);
                        l.setTitle("Loing Fehlgeschalgen");
                        l.setHeaderText("Ihr Konto ist gesperrt oder noch nicht aktiv");
                        l.setContentText("Bitte informieren Sie einen unserer Mitarbeiter");
                        l.show();
                    });
                    locked.setValue(true);
                    doneSignal.countDown();
                }
            };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return !locked.get();
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Fehlerhafter Login");
            alert.setHeaderText("Ihre Logindaten sind falsch");
            alert.setContentText("Bitte überprüfen Sie Ihre Eingaben");
            alert.show();

            removeListner(consume);
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            checkConnection();
            e.printStackTrace();
        }
        return false;
    }

    public boolean changePIN(String pin) {
        if (!checkConnection()) {
            return false;
        }
        PinChange request = new PinChange(pin);
        try {
            oout.writeObject(request);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consumer = (Request r) -> {
                if (r != null && r instanceof PinChange) {
                    System.out.println("Pinänderung empfangen");

                    PinChange s = (PinChange) r;
                    System.out.println("Pin: " + pin);
                    System.out.println("server: " + s.getPin());
                    if (Objects.equals(pin, s.getPin())) {
                        doneSignal.countDown();
                    }
                }
            };
            addListner(consumer);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consumer);
                return true;
            }
            removeListner(consumer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            reconnect();
        }
        return false;
    }

    /**
     * @return ein Paar aus Kononummer und Pin;
     */
    public Pair<Integer, String> register(String name, String vorname, String strasse, int nummer, String plz, String ort) {
        if (!checkConnection()) {
            return null;
        }
        String pin = "";
        for (int i = 0; i < 4; i++) {
            pin += new Random().nextInt(9);
        }
        Register request = new Register(name, vorname, plz, ort, strasse, nummer, pin);
        try {
            oout.writeObject(request);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            final IntegerProperty i = new SimpleIntegerProperty();
            Consumer<Request> consume = (Request r) -> {
                if (r != null && r instanceof OK) {
                    i.setValue(((OK) r).getKnr());
                    System.out.println("Register empfangen");
                    doneSignal.countDown();
                }
            };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return new Pair<>(i.get(), pin);
            }
            removeListner(consume);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            reconnect();
        }
        return null;
    }

    /**
     * Methode um an die Konten des Users zu kommen
     *
     * @return Die String Repr#sentationen der Konten.
     */
    public ArrayList<String> getKonten() {
        if (!checkConnection()) {
            return new ArrayList<>();
        }
        Ueberweisung ueberweisung = new Ueberweisung();
        try {
            oout.writeObject(ueberweisung);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);

            Consumer<Request> consume = (Request r) -> {
                System.out.println("Ueberweisungs objekt empfangen");
                if (r != null && r instanceof Ueberweisung) {
                    Ueberweisung u = (Ueberweisung) r;
                    ueberweisung.setKonten(u.getKonten());
                    doneSignal.countDown();
                }
            };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return ueberweisung.getKonten();
            }
            removeListner(consume);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            reconnect();
        }
        return null;
    }

    /**
     * Überweist eine Betrag auf ein anderes Konto. Im Falle eines Fehlers wird über ein Allert die Fehlermeldung angezeigt.
     *
     * @param kontoFrom Eigenes Ursprungskonto
     * @param kontoTo   Ziel Konto
     * @param betrag    Betrag der überwiesen werden soll
     * @param zweck     Verwendungshinweis der Überweisung
     * @return true wenn erfolgreich, ansonsten false
     */
    public boolean ueberweise(int kontoFrom, int kontoTo, double betrag, String zweck) {
        if (!checkConnection()) {
            return false;
        }
        Ueberweisung ueberweisung = new Ueberweisung(kontoFrom, kontoTo, betrag, zweck, null);
        try {
            oout.writeObject(ueberweisung);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consume =
                    (Request r) -> {
                        if (r != null && r instanceof Ueberweisung) {
                            Ueberweisung u = (Ueberweisung) r;
                            if (u.getKontonrVon() == -1) {
                                BankHelper.kontoEmpty();
                                return;
                            }
                            if (u.getKontonrZu() == -1) {

                                Platform.runLater(() -> {
                                    Alert alert = new Alert(Alert.AlertType.WARNING);
                                    alert.setTitle("Fehler bei der Überweisung");
                                    alert.setHeaderText("Zielkontonummer existiert nicht");
                                    alert.setContentText("Bitte überprüfen Sie ihre Eingaben");
                                    alert.show();
                                });
                                return;
                            }
                            doneSignal.countDown();
                        }
                    };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return false;
            }
            removeListner(consume);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            reconnect();
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Bittet den Server einen Betrag auszuzahlen. Wenn das Konto nicht ausreichend gedeckt ist, wird eine Warnung angezeigt.
     *
     * @param betrag  Abzuhebender Betrag
     * @param kontonr Konto von dem Abgehoben werden soll.
     * @return true wenn erfolgreich ansonsten false
     */
    public boolean zahleAus(double betrag, int kontonr) {
        if (!checkConnection()) {
            reconnect();
        }
        Auszahlen aus = new Auszahlen(betrag, kontonr);
        try {
            oout.writeObject(aus);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consume = (Request r) -> {
                if (r != null && r instanceof OK) {
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Auszahlen erfolgreich");
                        alert.setHeaderText("Der Auszahlungsvorgang war erfolgreich");
                        alert.setContentText("Sie werden auf die Startseite weitergeleitet");
                        alert.show();
                    });
                    doneSignal.countDown();
                } else if (r != null && r instanceof Fail) {
                    BankHelper.kontoEmpty();
                }
            };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return true;
            }
            removeListner(consume);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            reconnect();
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public boolean zahleEin(int betrag, int kontonr) {
        if (!checkConnection()) {
            reconnect();
        }
        Einzahlen aus = new Einzahlen(betrag, kontonr);
        try {
            oout.writeObject(aus);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consume =
                    (Request r) -> {
                        if (r != null && r instanceof OK) {
                            System.out.println("Ueberweisung erfolgreich");
                            doneSignal.countDown();
                            Platform.runLater(() -> {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setTitle("Einzahlen erfolgreich");
                                alert.setHeaderText("Der Einzahlungsvorgang war erfolgreich");
                                alert.setContentText("Sie werden auf die Startseite weitergeleitet");
                                alert.show();
                            });

                        }
                    };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return true;
            }
            removeListner(consume);
            System.out.println("Anfrage ausgetimed");
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            reconnect();
            e.printStackTrace();
            return false;
        }
        return false;
    }


    private boolean checkConnection() {
        if (s == null || s.isClosed() || oout == null) {
            reconnect();
        }
        if (s == null || s.isClosed() || oout == null) {
            alert();
        }
        return s != null;
    }

    public ArrayList<Transaktion> orderTransaktions() {
        checkConnection();
        Print p = new Print();
        try {
            oout.writeObject(p);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consume =
                    (Request r) -> {
                        if (r != null && r instanceof Print) {
                            Print print = (Print) r;
                            System.out.println("print empfangen: " + print.getHistory().size());
                            ArrayList<Transaktion> transaktions = print.getHistory();
                            Collections.sort(transaktions);
                            p.setTransaktionen(transaktions);
                            doneSignal.countDown();
                        }
                    };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return p.getHistory();
            }
            removeListner(consume);
            System.out.println("Anfrage ausgetimed");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            reconnect();
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public MitarbeiterView getKontenObjekte() {
        if (!checkConnection()) {
            reconnect();
        }
        MitarbeiterView mitarbeiterView = new MitarbeiterView();
        try {
            oout.writeObject(mitarbeiterView);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consume =
                    (Request r) -> {
                        if (r != null && r instanceof MitarbeiterView) {
                            System.out.println("Mitarbeiterview erhalten");
                            MitarbeiterView view = (MitarbeiterView) r;
                            mitarbeiterView.setKonten(view.getKonten());
                            mitarbeiterView.setKunden(view.getKunden());
                            doneSignal.countDown();
                        }
                    };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return mitarbeiterView;
            }
            removeListner(consume);
            System.out.println("Anfrage ausgetimed");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            reconnect();
            e.printStackTrace();
        }
        return new MitarbeiterView();
    }

    public boolean refreshKonten(MitarbeiterView mitarbeiterView) {
        if (!checkConnection()) {
            reconnect();
        }
        try {
            for (VO_Konto k : mitarbeiterView.getKonten()) {
                System.out.println("Konto " + k.kontoNr() + " ist aktiv " + k.active());
            }
            oout.writeObject(mitarbeiterView);
            oout.reset();
            CountDownLatch doneSignal = new CountDownLatch(1);
            Consumer<Request> consume =
                    (Request r) -> {
                        if (r != null && r instanceof OK) {
                            System.out.println("OK erhalten");
                            doneSignal.countDown();
                        }
                    };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                return true;
            }
            removeListner(consume);
            System.out.println("Anfrage ausgetimed");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            reconnect();
            e.printStackTrace();
        }
        return false;
    }

    public void disconnect() {
        System.out.println("Verbindung beenden");
        try {
            instance = null;
            oout.close();
            oin.close();
            in.close();
            out.close();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public String createKonto() {
        if (!checkConnection()) {
            reconnect();
        }
        try {
            oout.writeObject(new KontoRequest());
            CountDownLatch doneSignal = new CountDownLatch(1);
            IntegerProperty kontonr = new SimpleIntegerProperty(-1);
            Consumer<Request> consume =
                    (Request r) -> {
                        if (r != null && r instanceof OK) {
                            System.out.println("OK erhalten");
                            kontonr.setValue(((OK) r).getKnr());
                            doneSignal.countDown();
                        }
                    };
            addListner(consume);
            if (doneSignal.await(500, TimeUnit.MILLISECONDS)) {
                removeListner(consume);
                System.out.println("Konto erfolgreich hinzugefügt");
                return String.valueOf(kontonr.get());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            reconnect();
        }
        return null;
    }
}
