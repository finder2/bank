package de.schule.bank.core.vo.request;

/**
 * Created by user on 31.05.2016.
 */
public class Login extends Request {
    private final int kundennr;
    private final String pin;

    public Login(int kundennr, String pin) {
        this.kundennr = kundennr;
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public int getKundennr() {

        return kundennr;
    }
}
