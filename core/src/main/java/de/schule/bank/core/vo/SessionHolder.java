package de.schule.bank.core.vo;

/**
 * Erstellt von Daniel am 30.06.2016.
 */
public class SessionHolder {
    private static String pin;
    private static int kundennr;

    public SessionHolder(String pinP, int kundennrP) {
        pin = pinP;
        kundennr = kundennrP;
    }

    public static String getPin() {
        return pin;
    }

    public static void setPin(String pinP) {
        pin = pinP;
    }

    public static int getKundennr() {
        return kundennr;
    }

    public static void setKundennr(int kundennrP) {
        kundennr = kundennrP;
    }
}
