package de.schule.bank.core.vo;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Erstellt von Daniel am 05.07.2016.
 * Wird für die TableView benötigt
 */
public class TO_Konto {
    private static final long serialVersionUID = 0L;
    private final StringProperty kontoNr;
    private final StringProperty kontostand;
    private final BooleanProperty active;

    public TO_Konto(Integer kontoNr, Double kontostand, Boolean active) {
        this.kontoNr = new SimpleStringProperty(String.valueOf(kontoNr));
        this.kontostand = new SimpleStringProperty(String.valueOf(kontostand));
        this.active = new SimpleBooleanProperty(active);
    }


    public StringProperty kontoNrProperty() {
        return kontoNr;
    }

    public StringProperty kontostandProperty() {
        return kontostand;
    }


    public BooleanProperty activeProperty() {
        return active;
    }

}
