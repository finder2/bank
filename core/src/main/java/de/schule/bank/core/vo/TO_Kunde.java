package de.schule.bank.core.vo;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Erstellt von Daniel am 05.07.2016.
 */
public class TO_Kunde {
    private final StringProperty kundenNr;
    private final StringProperty name;
    private final StringProperty vorname;
    private final StringProperty strasse;
    private final StringProperty nr;
    private final StringProperty plz;
    private final StringProperty ort;
    private final BooleanProperty locked;
    private final BooleanProperty active;

    public TO_Kunde(Integer kundenNr, String name, String vorname, String strasse, Integer nr, String plz, String ort, Boolean locked, Boolean active) {
        this.kundenNr = new SimpleStringProperty(String.valueOf(kundenNr));
        this.name = new SimpleStringProperty(name);
        this.vorname = new SimpleStringProperty(vorname);
        this.strasse = new SimpleStringProperty(strasse);
        this.nr = new SimpleStringProperty(String.valueOf(nr));
        this.plz = new SimpleStringProperty(plz);
        this.ort = new SimpleStringProperty(ort);
        this.locked = new SimpleBooleanProperty(locked);
        this.active = new SimpleBooleanProperty(active);
    }

    public StringProperty kundenNrProperty() {
        return kundenNr;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty vornameProperty() {
        return vorname;
    }

    public StringProperty strasseProperty() {
        return strasse;
    }

    public StringProperty nrProperty() {
        return nr;
    }

    public StringProperty plzProperty() {
        return plz;
    }

    public StringProperty ortProperty() {
        return ort;
    }

    public BooleanProperty lockedProperty() {
        return locked;
    }

    public BooleanProperty activeProperty() {
        return active;
    }

}
