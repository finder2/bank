package de.schule.bank.core.vo;

import java.io.Serializable;

/**
 * Erstellt von Daniel am 05.07.2016.
 * Wird für die Server-Mitarbeiter Kommunikation benötigt
 */
public class VO_Konto implements Serializable {
    private static final long serialVersionUID = 0L;
    private final Integer kontoNr;
    private final Double kontostand;
    private final Boolean active;

    public VO_Konto(Integer kontoNr, Double kontostand, Boolean active) {
        System.out.println("neues konto ist " + active);
        this.kontoNr = kontoNr;
        this.kontostand = kontostand;
        this.active = active;
        System.out.println("neuer kontostand ist " + this.kontostand);
    }


    public Integer kontoNr() {
        return kontoNr;
    }

    public Double kontostand() {
        return kontostand;
    }


    public Boolean active() {
        return active;
    }

}
