package de.schule.bank.core.vo;

import java.io.Serializable;

/**
 * Erstellt von Daniel am 05.07.2016.
 * Wird für die Server-Mitarbeiter Kommunikation benötigt
 */
public class VO_Kunde implements Serializable {
    private static final long serialVersionUID = 0L;
    private final Integer kundenNr;
    private final String name;
    private final String vorname;
    private final String strasse;
    private final Integer nr;
    private final String plz;
    private final String ort;
    private final Boolean locked;
    private final Boolean active;

    public VO_Kunde(Integer kundenNr, String name, String vorname, String strasse, Integer nr, String plz, String ort, Boolean locked, Boolean active) {
        this.kundenNr = kundenNr;
        this.name = name;
        this.vorname = vorname;
        this.strasse = strasse;
        this.nr = nr;
        this.plz = plz;
        this.ort = ort;
        this.locked = locked;
        this.active = active;
    }

    public Integer kundenNr() {
        return kundenNr;
    }

    public String name() {
        return name;
    }

    public String vorname() {
        return vorname;
    }

    public String strasse() {
        return strasse;
    }

    public Integer nr() {
        return nr;
    }

    public String plz() {
        return plz;
    }

    public String ort() {
        return ort;
    }

    public Boolean locked() {
        return locked;
    }

    public Boolean active() {
        return active;
    }

}
