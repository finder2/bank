package de.schule.bank.core.vo.request;

/**
 * Erstellt von Daniel am 03.07.2016.
 */
public class Auszahlen extends Request implements Transaktion {
    private static final long serialVersionUID = 0L;
    private final Integer konto;
    private final Double betrag;

    public Auszahlen(double betrag, int konto) {
        this.konto = konto;
        this.betrag = betrag;

    }

    @Override
    public int getKonto() {
        return konto;
    }

    @Override
    public double getBetrag() {
        return betrag;
    }

    @Override
    public String getDescription() {
        return "Auszahlung";
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Transaktion)) {
            throw new ClassCastException("Kann nicht zu Print gecastet werden");
        }
        Transaktion otherPrint = (Transaktion) o;
        return otherPrint.getCal().compareTo(getCal());
    }
}
