package de.schule.bank.core.vo.request;

import de.schule.bank.core.vo.TO_Konto;
import de.schule.bank.core.vo.TO_Kunde;
import de.schule.bank.core.vo.VO_Konto;
import de.schule.bank.core.vo.VO_Kunde;
import javafx.collections.ObservableList;

import java.util.ArrayList;

/**
 * Erstellt von Daniel am 05.07.2016.
 */
public class MitarbeiterView extends Request {
    private static final long serialVersionUID = 0L;
    private ArrayList<VO_Konto> konten = new ArrayList<>();
    private ArrayList<VO_Kunde> kunden = new ArrayList<>();

    public ArrayList<VO_Konto> getKonten() {
        return konten;
    }

    public void setKonten(ObservableList<TO_Konto> konten) {
        this.konten.clear();
        for (TO_Konto k : konten) {
            this.konten.add(new VO_Konto(Integer.parseInt(k.kontoNrProperty().get()), Double.parseDouble(k.kontostandProperty().get()), k.activeProperty().get()));
        }

    }

    public void setKonten(ArrayList<VO_Konto> konten) {
        this.konten = konten;
    }

    public void addKonto(TO_Konto k) {
        konten.add(new VO_Konto(Integer.parseInt(k.kontoNrProperty().get()), Double.parseDouble(k.kontostandProperty().get()), k.activeProperty().get()));
    }

    public ArrayList<VO_Kunde> getKunden() {
        return kunden;
    }

    public void setKunden(ObservableList<TO_Kunde> kunden) {
        this.kunden.clear();
        for (TO_Kunde k : kunden) {
            System.out.println("Kunde " + k.kundenNrProperty() + " ist akitiv: " + k.activeProperty().get());
            this.kunden.add(new VO_Kunde(Integer.parseInt(k.kundenNrProperty().get()), k.nameProperty().get(), k.vornameProperty().get(), k.strasseProperty().get(), Integer.parseInt(k.nrProperty().get()), k.plzProperty().get(), k.ortProperty().get(), k.lockedProperty().get(), k.activeProperty().get()));
        }
    }

    public void setKunden(ArrayList<VO_Kunde> kunden) {
        this.kunden = kunden;

    }

    public void addKunde(TO_Kunde kunde) {
        kunden.add(new VO_Kunde(Integer.parseInt(kunde.kundenNrProperty().get()), kunde.nameProperty().get(), kunde.vornameProperty().get(), kunde.strasseProperty().get(), Integer.parseInt(kunde.nrProperty().get()), kunde.plzProperty().get(), kunde.ortProperty().get(), kunde.lockedProperty().get(), kunde.activeProperty().get()));
    }
}
