package de.schule.bank.core.vo.request;

/**
 * Created by Daniel on 30.06.2016.
 */
public class OK extends Request {
    private final Integer knr;

    public OK(int i) {
        knr = i;
    }

    public int getKnr() {
        return knr;
    }
}
