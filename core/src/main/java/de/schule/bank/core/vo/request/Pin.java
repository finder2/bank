package de.schule.bank.core.vo.request;

/**
 * Created by user on 07.06.2016.
 */
public class Pin extends Request {
    private final int kundenNr;
    private final String newPin;

    public Pin(int kundenNr, String newPin) {
        this.kundenNr = kundenNr;
        this.newPin = newPin;
    }

    public int getKundenNr() {
        return kundenNr;
    }

    public String getNewPin() {
        return newPin;
    }
}
