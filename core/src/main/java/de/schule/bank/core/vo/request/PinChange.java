package de.schule.bank.core.vo.request;

/**
 * Created by Daniel on 30.06.2016.
 */
public class PinChange extends Request {
    private String pin;

    public PinChange(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }
}
