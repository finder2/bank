package de.schule.bank.core.vo.request;

import de.schule.bank.core.vo.request.transaction.Transaktion;

import java.util.ArrayList;

/**
 * Erstellt von user am 14.06.2016.
 */
public class Print extends Request {
    private ArrayList<Transaktion> transaktionen;

    public Print() {
        this.transaktionen = new ArrayList<>();
    }

    /**
     * @return null wenn es eine Anfrage an den Server war ansonsten entsprechender Wert
     */
    public ArrayList<Transaktion> getHistory() {
        return transaktionen;
    }

    public void addTransaktion(Transaktion t) {
        transaktionen.add(t);
    }

    public void setTransaktionen(ArrayList<Transaktion> transaktionen) {
        this.transaktionen = transaktionen;
    }
}
