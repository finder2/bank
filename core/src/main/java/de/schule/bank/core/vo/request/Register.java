package de.schule.bank.core.vo.request;

/**
 * Erstellt von user am 31.05.2016.
 */
public class Register extends Request {
    private String name;
    private String vorname;
    private String plz;
    private String ort;
    private String straße;
    private Integer nr;
    private String pin;

    public Register(String name, String vorname, String plz, String ort, String straße, int nr, String pin) {
        this.name = name;
        this.vorname = vorname;
        this.plz = plz;
        this.ort = ort;
        this.straße = straße;
        this.nr = nr;
        this.pin = pin;
    }

    public String getName() {
        return name;
    }


    public String getVorname() {
        return vorname;
    }

    public String getPlz() {
        return plz;
    }

    public String getOrt() {
        return ort;
    }


    public String getStraße() {
        return straße;
    }

    public int getNr() {
        return nr;
    }

    public String getPin() {
        return pin;
    }
}
