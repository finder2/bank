package de.schule.bank.core.vo.request.transaction;

import de.schule.bank.core.vo.request.Request;

/**
 * Erstellt von Daniel am 03.07.2016.
 */
public class Einzahlen extends Request implements Transaktion {
    private static final long serialVersionUID = 0L;
    private final Integer konto;
    private final Double betrag;

    public Einzahlen(double betrag, int konto) {
        this.konto = konto;
        this.betrag = betrag;

    }

    @Override
    public int getKonto() {
        return konto;
    }

    @Override
    public double getBetrag() {
        return betrag;
    }

    @Override
    public String getDescription() {
        return "Einzahlung";
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Transaktion)) {
            throw new ClassCastException("Kann nicht zu Print gecastet werden");
        }
        Transaktion otherPrint = (Transaktion) o;
        return otherPrint.getCal().compareTo(getCal());
    }
}
