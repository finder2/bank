package de.schule.bank.core.vo.request.transaction;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Erstellt von user am 07.06.2016.
 */
public interface Transaktion extends Comparable {
    Calendar cal = Calendar.getInstance();

    int getKonto();

    double getBetrag();

    String toString();

    default String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat();
        if (cal == null) {
            return null;
        }
        return sdf.format(cal.getTime());
    }

    default Calendar getCal() {
        return cal;
    }

    String getDescription();
}
