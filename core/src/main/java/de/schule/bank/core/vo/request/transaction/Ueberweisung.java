package de.schule.bank.core.vo.request.transaction;

import de.schule.bank.core.vo.request.Request;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Erstellt von user am 07.06.2016.
 */
public class Ueberweisung extends Request implements Transaktion {
    private static final long serialVersionUID = 0L;
    private final Integer kontonrVon;
    private final Integer kontonrZu;
    private final Double betrag;
    private final String verwendungszweck;
    private final Calendar cal;
    private ArrayList<String> konten;

    public Ueberweisung() {
        kontonrVon = null;
        kontonrZu = null;
        betrag = null;
        verwendungszweck = null;
        cal = null;
        konten = null;
    }

    public Ueberweisung(ArrayList<String> konten) {
        kontonrVon = null;
        kontonrZu = null;
        betrag = null;
        verwendungszweck = null;
        cal = null;
        this.konten = konten;
    }

    public Ueberweisung(int kontonrVon, int kontonrZu, double betrag, String verwendungszweck, ArrayList<String> konten) {
        this.kontonrVon = kontonrVon;
        this.kontonrZu = kontonrZu;
        this.betrag = betrag;
        this.verwendungszweck = verwendungszweck;
        this.cal = Calendar.getInstance();
        this.konten = konten;
    }

    @Override
    public String toString() {
        return "Am " + getDate() + " wurden " + betrag + "€ von Konto " + kontonrVon + " auf Konto " + kontonrZu + " überwiesen ";
    }

    public int getKontonrVon() {
        return kontonrVon;
    }

    public int getKontonrZu() {
        return kontonrZu;
    }

    @Override
    public int getKonto() {
        return kontonrVon;
    }

    public double getBetrag() {
        return betrag;
    }

    public String getVerwendungszweck() {
        return verwendungszweck;
    }

    @Override
    public String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat();
        if (cal == null) {
            return null;
        }
        return sdf.format(cal.getTime());
    }

    @Override
    public String getDescription() {
        return "Überweisung an " + kontonrZu;
    }

    public ArrayList<String> getKonten() {
        return konten;
    }

    public void setKonten(ArrayList<String> konten) {
        this.konten = konten;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Transaktion)) {
            throw new ClassCastException("Kann nicht zu Print gecastet werden");
        }
        Transaktion otherPrint = (Transaktion) o;
        return otherPrint.getCal().compareTo(getCal());
    }
}
